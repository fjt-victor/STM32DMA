/*
 * @Descripttion: 
 * @version: 
 * @Author: MichaelHu
 * @Date: 2020-06-18 15:46:33
 * @LastEditors: MichaelHu
 * @LastEditTime: 2020-09-23 11:23:17
 */
#include "FIFO.h"
#include "delay.h"
#include "usart_data_process.h"
#include "usart_dma.h"

u8 test_buf[100];
u16 len;

int main(void)
{
    u8 res = 0;
    delay_init();
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
    TIM3_Int_Init(1000 - 1, 72 - 1); //1MHZ 1000次  1ms计时
    usart_dma_init();

    printf("start\r\n");
    for (;;)
    {
        res = usart1_Process(test_buf, &len);
        if (res == 0)
        {
            res = 1;
            for (int i = 1; i < len; i++)
            {
                while ((USART1->SR & 0X40) == 0) {}; //循环发送,直到发送完毕
                USART1->DR = (u8)test_buf[i];
            }
        }
    }
}
