#include "DMA.h"

//DMA_InitTypeDef DMA_InitStructure;
//u8 DMA_TC_IT_flag = 0;

//void MYDMA_Config(DMA_Channel_TypeDef* DMA_CHx,u32 cpar,u32 cmar,u16 cndtr)
//{
//    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);//使能DMA时钟

//    DMA_DeInit(DMA_CHx);
//    DMA_InitStructure.DMA_PeripheralBaseAddr = cpar;//外设地址
//    DMA_InitStructure.DMA_MemoryBaseAddr = cmar; //内存地址
//    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC; //传输方向外设到内存
//    DMA_InitStructure.DMA_BufferSize = cndtr;  //传输量
//    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;//外设地址不自增
//    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable; //内存地址自增
//    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
//    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
//    DMA_InitStructure.DMA_Mode = DMA_Mode_Circular; //循环模式
//    DMA_InitStructure.DMA_Priority = DMA_Priority_High;//高优先级
//    DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
//    DMA_Init(DMA_CHx, &DMA_InitStructure);
//	
//	/*中断*/     
//	NVIC_InitTypeDef NVIC_InitStructure;       
//	NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel5_IRQn;     
//	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
//	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
//	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;          
//	NVIC_Init(&NVIC_InitStructure);
//	
//	DMA_ITConfig(DMA1_Channel5,DMA_IT_TC,ENABLE);  //配置DMA发送完成后产生中断
//}

//void DMA1_Channel5_IRQHandler(void)
//{
//    if(DMA_GetITStatus(DMA1_IT_TC5))
//    {
//        DMA_ClearITPendingBit(DMA1_IT_GL5); 
//		DMA_TC_IT_flag++;
//    }
//}

