/*
 * @Descripttion: 
 * @version: 
 * @Author: MichaelHu
 * @Date: 2020-06-18 18:14:14
 * @LastEditors: MichaelHu
 * @LastEditTime: 2020-12-02 11:38:50
 */
#ifndef __FIFO_H
#define __FIFO_H
#include "sys.h"

#define FIFO_OK                  0
#define FIFO_ERROR_PARAM         1
#define FIFO_ERROR_MEM           2
#define FIFO_ERROR_FULL          3
#define FIFO_ERROR_EMPTY         4
#define FIFO_ERROR_BUSY          5

#define FIFO_BUF_LENGTH			1000	
#define FIFO_DMA		        1  //����DMAģʽ

typedef struct
{
    u32 size;                      //FIFO buffer size
    u32 front;                     //FIFO next read position
    u32 staraddr;                  //FIFO buffer start address
    u32 endaddr;                   //FFIFO buffer end address
	u32 CNDTR;					   //Received data count
    u8 *buffer;               
} FIFOTYPE;

u32 FIFO_Init(FIFOTYPE **fifo, u32 fifosize, u8 dma_flag);
u32 FIFO_Clear(FIFOTYPE *fifo, u8 dma_flag);
u32 FIFO_Read(FIFOTYPE *fifo, u8 *data, u8 mode, u8 dma_flag);
u32 FIFO_ReadN(FIFOTYPE *fifo, u8 *data, u16 length, u8 dma_flag);
u32 FIFO_Status(FIFOTYPE *fifo, u8 dma_flag);
u32 FIFO_Remove(FIFOTYPE *fifo, u32 RemoveNum);

#endif
