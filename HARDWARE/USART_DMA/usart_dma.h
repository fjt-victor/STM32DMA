/*
 * @Descripttion: 
 * @version: 
 * @Author: MichaelHu
 * @Date: 2020-09-17 09:58:39
 * @LastEditors: MichaelHu
 * @LastEditTime: 2020-09-17 10:09:16
 */
#ifndef __USART_DMA_H
#define __USART_DMA_H

#include "sys.h"

void usart_dma_init(void);
void MYDMA_Config(DMA_Channel_TypeDef *DMA_CHx, u32 cpar, u32 cmar, u16 cndtr);
#endif
