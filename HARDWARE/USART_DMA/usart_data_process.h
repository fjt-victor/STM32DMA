/*
 * @Descripttion: 
 * @version: 
 * @Author: MichaelHu
 * @Date: 2020-09-17 09:58:39
 * @LastEditors: MichaelHu
 * @LastEditTime: 2020-09-23 11:09:15
 */
#ifndef __USART_DATA_PROCESS_H
#define __USART_DATA_PROCESS_H

#include "sys.h"

void TIM3_Int_Init(u16 arr, u16 psc);
u8 usart1_Process(u8 *data, u16 *data_length);
#endif
