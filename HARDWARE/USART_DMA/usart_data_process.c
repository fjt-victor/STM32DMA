#include "usart_data_process.h"
#include "FIFO.h"

u16 time3_cnt = 0;
u8 read_Flag = 0;       //用于接收数据超时判断
static u8 data_len = 0; //数据长度
extern FIFOTYPE *uartFIFO;

void TIM3_Int_Init(u16 arr, u16 psc)
{
    TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);

    TIM_TimeBaseInitStructure.TIM_Prescaler = psc; //分频值
    TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInitStructure.TIM_Period = arr; //自动重装数值
    TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
    TIM_TimeBaseInit(TIM3, &TIM_TimeBaseInitStructure);
    TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);

    NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    TIM_Cmd(TIM3, ENABLE);
}

void TIM3_IRQHandler(void)
{
    if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET)
    {
        TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
        time3_cnt++;
        if (read_Flag) //超过时间还没有收到数据，则清零标志位
        {
            if ((data_len > FIFO_Status(uartFIFO, 1)) && (time3_cnt >= 5)) //1M波特率，一个数据10us，超过500*10us=5ms则算超时，根据不同数据长度选择时间
            {
                read_Flag = 0;
                //printf("ERROR DATA FIFO LEN = %d\r\n", FIFO_Status(uartFIFO));
            }
        }
    }
}

/*数据包处理*/
/*数据格式*/
/*头码55 55，长度包含除头码之外的所有字节*/
//  头码    |  长度  |  命令  |  数据  |  校验
//  55 55      03       01       01      无
u8 usart1_Process(u8 *data, u16 *data_length)
{
    u8 read_Data;
    if (read_Flag == 1) //收到头码，等待数据接收完成，超时则清零
    {
        if (FIFO_Status(uartFIFO, 1) >= data_len)
        {
            /*处理数据*/
            FIFO_ReadN(uartFIFO, data, data_len, 1);
            *data_length = data_len;
            read_Flag = 0; //清除标志
            return 0;
        }
    }
    else
    {
        /*接收头码*/
        if (FIFO_Status(uartFIFO, 1) >= 6) //最小一帧数据6字节
        {
            read_Data = 0;
            FIFO_Read(uartFIFO, &read_Data, 1, 1);
            if (read_Data != 0x55)
            {
                return 1;
            }
            else
            {
                read_Data = 0;
                FIFO_Read(uartFIFO, &read_Data, 1, 1);
                if (read_Data != 0x55)
                {
                    return 1;
                }
                else
                {
                    data_len = 0;
                    FIFO_Read(uartFIFO, &data_len, 0, 1);
                    if (FIFO_Status(uartFIFO, 1) >= data_len)
                    {
                        /*处理数据*/
                        FIFO_ReadN(uartFIFO, data, data_len, 1);
                        *data_length = data_len;
                        read_Flag = 0; //清除标志
                        return 0;
                    }
                    else
                    {
                        time3_cnt = 0;
                        read_Flag = 1; //数据没有接收完成，标志置位，等待定时器中判断
                        return 1;
                    }
                }
            }
        }
    }
    return 1;
}
